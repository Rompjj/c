#include <stdio.h>
#include <stdlib.h>

    typedef struct Pi{
        int item;
        struct Pi *ant;
    }Tpilha;

    typedef struct {
        Tpilha *topo;
    }pilha;

void cria(pilha *p)
{
    p->topo = NULL;
}

int pilha_vazia(pilha *p)
{
    if(p->topo == NULL)
    {
        return 1;
    }else {
        return 0;
    }
}

void empilhar(pilha *p, int valor)
{
    Tpilha *novo = (Tpilha*)malloc(sizeof(Tpilha));
    if(novo == NULL)
    {
        printf("\n Alocacao nao foi feita.\n");
    }else{

        novo->item = valor;
        novo->ant = p->topo;
        p->topo = novo;
    }
}

int desempilha(pilha *p)
{
    Tpilha *aux = (Tpilha*)malloc(sizeof(Tpilha));
    int x;

    aux = p->topo;
    x = aux->item;
    p->topo = aux->ant;
    aux->ant = NULL;

    free(aux);

    return x;
}

void mostrar(pilha *p, int num)
{
    Tpilha *aux = (Tpilha*)malloc(sizeof(Tpilha));
    int x;
    printf("\n============================\n",num);
    printf("\n\n  ---- Pilha %d ------- \n\n",num);
    aux = p->topo;
    while(aux != NULL)
    {
        x = aux->item;
        printf("\n >%d",x);
        aux = aux->ant;
    }
}
int mostra_topo(pilha *p)
{
    int i;
   Tpilha *aux;

   aux=p->topo;
   i = aux->item;
   printf("valor desempilhado Topo : %d \n",i);

   return i;
}

void menu()
{
    pilha *pa = (pilha*)malloc(sizeof(pilha));
    cria(pa);

    pilha *pb = (pilha*)malloc(sizeof(pilha));
    cria(pb);

    pilha *pc = (pilha*)malloc(sizeof(pilha));
    cria(pc);

    pilha *pd = (pilha*)malloc(sizeof(pilha));
    cria(pd);

    empilhar(pa, 4);
    empilhar(pa, 3);
    empilhar(pa, 2);
    empilhar(pa, 1);

    int opcao, retira, poem,valor;

   do{
        system("cls");
        mostrar(pa,1);
        mostrar(pb,2);
        mostrar(pc,3);

       printf("\n menu ======================\n");
       printf(" 1 - desempilhar\n");
       printf(" 2 - sair");
       printf("\n Escolha a opcao..: ");
       scanf("%d",&opcao);

       switch(opcao)
       {
        case 1:
            printf("\n qual pilha deseja desempilha..: 1pa - 2pb - 3pc \t");
            scanf("%d",&retira);

            printf("\n qual pilha deseja empilhar..: 1pa - 2pb - 3pc \t");
            scanf("%d",&poem);

            if(retira == 1 && poem == 2)
            {
                if(pilha_vazia(pa))
                {
                    printf("\n Pilha A vazia.\n");
                }else{
                    valor = desempilha(pa);
                    empilhar(pb,valor);
                    printf("\n\n  Topo pb: %d \n",mostra_topo(pb));
                }
            }else if(retira == 1 && poem == 3)
            {
                if(pilha_vazia(pa))
                    {
                        printf("\n Pilha A vazia.\n");
                    }else{
                        valor = desempilha(pa);
                        empilhar(pc,valor);
                        printf("\n\n  Topo pb: %d \n",mostra_topo(pc));
                    }
            }else if(retira == 2 && poem == 1)
            {
                if(pilha_vazia(pb))
                    {
                        printf("\n Pilha A vazia.\n");
                    }else{
                        valor = desempilha(pb);
                        empilhar(pa,valor);
                        printf("\n\n  Topo pb: %d \n",mostra_topo(pa));
                    }
            }else if(retira == 2 && poem == 3)
            {
                if(pilha_vazia(pb))
                    {
                        printf("\n Pilha A vazia.\n");
                    }else{
                        valor = desempilha(pb);
                        empilhar(pc,valor);
                        printf("\n\n  Topo pb: %d \n",mostra_topo(pc));
                    }
            }else if(retira == 3 && poem == 1)
            {
                if(pilha_vazia(pc))
                    {
                        printf("\n Pilha A vazia.\n");
                    }else{
                        valor = desempilha(pc);
                        empilhar(pa,valor);
                        printf("\n\n  Topo pb: %d \n",mostra_topo(pa));
                    }
            }else if(retira == 3 && poem == 2)
            {
                if(pilha_vazia(pc))
                    {
                        printf("\n Pilha A vazia.\n");
                    }else{
                        valor = desempilha(pc);
                        empilhar(pb,valor);
                        printf("\n\n  Topo pb: %d \n",mostra_topo(pb));
                    }
            }
            break;
        case 2:
            opcao = 3;
            break;

        }
    }while(opcao != 3);
}



int main()
{
   menu();
    return 0;
}
