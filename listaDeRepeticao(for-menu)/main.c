#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    int opc, num;

    do{
        system("cls");
        printf("==== Menu ========");
        printf("\n 0 - sair ");
        printf("\n escolha de 1 a 14: \t");
        fflush(stdin);
        scanf("%d",&opc);


        switch(opc)
        {
        case 0:
            opc = 15;
            break;

        case 1:
            printf("\n ==== OPCAO 1 ========");
            printf("\n escolha um valhor que seja par: \t");
            fflush(stdin);
            scanf("%d",&num);

            while(num%2 != 0)
            {
                printf("\n escolha um novo valhor que seja par: \t");
                fflush(stdin);
                scanf("%d",&num);
            }

            for(int i = 0; i <= num; i+=2)
            {
                printf("\n-> %d",i);
            }
            printf("\n");
            system("pause");
            break;

        case 2:
            printf("\n ==== OPCAO 2 ========");

            printf("\n escolha um valhor que seja impar: \t");
            fflush(stdin);
            scanf("%d",&num);

            while(num%2 == 0)
            {
                printf("\n escolha um novo valhor que seja impar: \t");
                fflush(stdin);
                scanf("%d",&num);
            }

            for(int i = 1; i <= num; i+=2)
            {
                printf("\n-> %d",i);
            }
            printf("\n");
            system("pause");
            break;
        case 3:
            printf("\n ==== OPCAO 3 ========");

            printf("\n escolha um valhor multiplo de 5 para finalizar: \t");
            fflush(stdin);
            scanf("%d",&num);

            while(num%5 != 0)
            {
                printf("\n escolha um novo valhor multiplo de 5 para finalizar: \t");
                fflush(stdin);
                scanf("%d",&num);
            }

            for(int i = 5; i <= num; i+=5)
            {
                printf("\n-> %d",i);
            }
            printf("\n");
            system("pause");
            break;

        case 4:
            printf("\n ==== OPCAO 4 ========");

            printf("\n escolha um valhor \t");
            fflush(stdin);
            scanf("%d",&num);

            for(int i = num; i >= 0; i--)
            {
                printf("\n-> %d",i);
            }
            num = num *-1; //passa valor para negativo

            for(int i = -1; i >= num; i--)
            {
               printf("\n-> %d",i);
            }

            printf("\n");
            system("pause");
            break;

        case 5:
            printf("\n ==== OPCAO 5 ========");

            printf("\n escolha um valhor multiplo de 2 para finalizar: \t");
            fflush(stdin);
            scanf("%d",&num);

            while(num%2 != 0)
            {
                printf("\n escolha um novo valhor multiplo de 2 para finalizar: \t");
                fflush(stdin);
                scanf("%d",&num);
            }

            for(int i = 2; i <= num; i *= 2)
            {
                printf("\n-> %d",i);
            }
            printf("\n");
            system("pause");
            break;

        case 6:
            printf("\n ==== OPCAO 6 ========");

            printf("\n escolha um valhor multiplo de 3 para finalizar: \t");
            fflush(stdin);
            scanf("%d",&num);

            while(num%3 != 0)
            {
                printf("\n escolha um novo valhor multiplo de 3 para finalizar: \t");
                fflush(stdin);
                scanf("%d",&num);
            }

            for(int i = 1; i <= num; i *= 3)
            {
                printf("\n-> %d",i);
            }
            printf("\n");
            system("pause");
            break;

        case 7:
            printf("\n ==== OPCAO 7 ========");

            printf("\n escolha um valhor positivo: \t");
            fflush(stdin);
            scanf("%d",&num);

            while(num <= 0)
            {
                printf("\n escolha um valhor positivo: \t");
                fflush(stdin);
                scanf("%d",&num);
            }

            int i = 1;
            int num2 = 4;

            for(i = 1; i <= num; i++)
            {
                if(i == num2)
                {
                    printf("\n-> %d",i);
                    num2 = i*2;
                    i = num2;
                   // printf("\n-> %d",i);
                }

                printf("\n-> %d",i);
                getch();


            }
            printf("\n");
            system("pause");
            break;


        }
    }while(opc > 0 && opc < 15);


    return 0;
}
