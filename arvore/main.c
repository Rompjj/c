#include <stdio.h>
#include <stdlib.h>

    typedef struct no{
        int dado;
        struct no *esquerda;
        struct no *direita;
    }no;
//funcao INSERIR retorna um no.
no *inserir(no *raiz,int num)
{
    //se esta sem raiz logo esta vazia
    if(raiz == NULL){
        no *novo = (no*)malloc(sizeof(no));
        if(novo == NULL){
            printf("\n Alocacao nao foi feita. \n");
        }else{
            novo->dado = num;               //recebe valor
            novo->direita = NULL;           //cria lado direito
            novo->esquerda = NULL;          //cria lado esquerda
            raiz = novo;                    //cria raiz da nova sub arvore
        }
    }else{     //caso nao esteja vazia
        if(num == raiz->dado)              //valor inserido ja existe
            printf("\n O numero ja foi inserido. \n");
        if(num > raiz->dado)               //num maior logo caminha e inseri a direita
            raiz->direita = inserir(raiz->direita,num); //chamo inserir recursivo
        if(num < raiz->dado)               //num menor logo caminha e inseri a esquerda
            raiz->esquerda = inserir(raiz->esquerda,num);
    }
    return (raiz);
}

void preOrdem(no *raiz)// Come�a pela raiz e vai printando at� o �ltimo a esquerda, depois volta pra raiz e mostra ate o ultimo a esquerda com execao dos que ja foi mostrado antes, volta denovo a buscar o �ltimo a esquerda e segue o fluxo.
{
    if(raiz != NULL)
    {
        printf("%d \n ",raiz->dado);
        preOrdem(raiz->esquerda);//chamada recursiva pro n� a esquerda, perceba que o pRaiz->direita s� � chamado quando passa por todos os n�s a esquerda
        preOrdem(raiz->direita);//chamada recursiva para n� � direita
    }
}

void emOrdem(no *raiz)//busca o �ltimo � esquerda, depois volta at� o n� onde ele ter� que ir � direita. Ap�s isso ele busca o �ltimo � esquerda e volta....
{
    if(raiz != NULL)
    {
        emOrdem(raiz->esquerda); // recursiva para o pr�ximo � esquerda, e ser� chamado at� o �ltimo � esquerda.
        printf("%d \n ",raiz->dado);
        emOrdem(raiz->direita); // direita, seguindo o fluxo
    }
}

void posOrdem(no *raiz) //A ideia basicamente � passar por toda a �rvore, e s� depois vir fazendo os prints. Ele busca o �ltimo a esquerda, depois volta pro n� e vai pra direita, sem printar nada, e busca de novo o �ltimo a esquerda, ate varrer toda a �rvore, depois ele vai printando tudo.
{ //mostra folha por folha
    if(raiz != NULL)
    {
        posOrdem(raiz->esquerda);
        posOrdem(raiz->direita);
        printf("%d \n ",raiz->dado);
    }
}

no* remover(no *raiz, int num)
{
    if(raiz == NULL){
        return NULL;
    }else if(num < raiz->dado){
        raiz->esquerda = remover(raiz->esquerda,num); //busca sub arvore da esquerda
    }else if(num > raiz->dado){
        raiz->direita = remover(raiz->direita,num); //busca sub arvore da direita
    }else{ //achou o valor a ser removido

        //no folha
        if(raiz->esquerda == NULL && raiz->direita == NULL){
            free(raiz);
            raiz = NULL;
        }else if(raiz->esquerda == NULL){  //no com filho a direita
            no* aux = raiz;
            raiz = raiz->direita;
            free(aux);
        }else if(raiz->direita == NULL){   //no com filho a esquerda
            no* aux = raiz;
            raiz = raiz->esquerda;
            free(aux);
        }else{
            no* aux = raiz->esquerda;  //aponta aux para a subarvore a esquerda

            while(aux->direita != NULL){
                aux = aux->direita;
            }

            raiz->dado = aux->dado;  //troca as informacoes
            aux->dado = num;
            //exclui o valor encontrado
            raiz->esquerda = remover(raiz->esquerda,num);
        }
    }
    return raiz;
}

no* buscar(no *raiz, int num)
{
    if (raiz == NULL){
        return NULL;
    }else if(raiz->dado > num){
            return buscar(raiz->esquerda,num);
        }else if(raiz->dado < num){
                return buscar(raiz->direita,num);
            }
            return raiz->dado;
}

void menu()
{
    int op,valor;  //op��o recursiva

    no *raiz=(no*)malloc(sizeof(no));
    raiz = NULL;                       //criando


     do{
        system("cls");
        printf("\n\n");
        printf("1 - Inserir Arvore \n");
        printf("2 - Remover Arvore \n");
        printf("3 - Mostrar Arvore pre_order \n");
        printf("4 - Mostrar Arvore in_order  \n");
        printf("5 - Mostrar Arvore pos_order \n");
        printf("6 - Buscar numero \n");// <<<<<<<fazer
        printf("7 - Sair \n ");

        printf("\n\n Informe a opcao :> ");
        scanf("%d",&op);

        switch(op)
        {
         case 1:
           printf("\n Informe o valor: >_");
           scanf("%d",&valor);

           raiz = inserir(raiz,valor);
           getch();
           break;
         case 2:
           printf("\n Informe o valor: >_");
           scanf("%d",&valor);

           raiz = remover(raiz,valor);
           getch();
           break;
         case 3:
          preOrdem(raiz);
          getch();
          break;
         case 4:
            emOrdem(raiz);
            getch();
            break;
        case 5:
            posOrdem(raiz);
            getch();
            break;
        case 6:
            printf("\n Informe o valor que procura: >_");
            scanf("%d",&valor);

            if(buscar(raiz,valor)!= 0)
            {
                printf("\n dado %d encontrado! \n",buscar(raiz,valor));
            }else{
                printf("\n valor nao encontrado!\n");
            }

            getch();
            break;
        case 7:
            op = 7;
            break;
        }
      }while(op != 7);
}


int main()
{
    menu();
    return 0;
}
