#include <stdio.h>
#include <stdlib.h>
// criar mostrar, fila e finalizar.

    typedef struct Pi{
        int item;
        struct Pi *ant;
    }Tpilha;

    typedef struct{
        Tpilha *topo;
    }pilha;

    typedef struct Fi{
        int dado;
        struct Fi *prox;
    }Tfila;

    typedef struct{
        Tfila *inicio;
        Tfila *fim;
    }fila;

void criar_pilha(pilha *p)
{
    p->topo = NULL;
}
void criar_fila(fila *f)
{
    f->inicio = NULL;
    f->fim = NULL;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
int pilha_vazia(pilha *p)
{
    if(p->topo == NULL)
    {
        return 1;
    }else{
        return 0;
    }
}

int fila_vazia(fila *f)
{
    if(f->inicio == NULL)
    {
        return 1;
    }else{
        return 0;
        }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
void empilhar(pilha *p,int valor)
{
    Tpilha *novo = (Tpilha*)malloc(sizeof(Tpilha));
    if(novo == NULL)
    {
        printf("\n alocacao nao foi feita.\n");
    }else{

        novo->item = valor;
        novo->ant = p->topo;
        p->topo = novo;
    }
}

void enfileirar(fila *f,int valor)
{
    Tfila *novo = (Tfila*)malloc(sizeof(Tfila));
    if(novo == NULL)
    {
        printf("\n alocacao nao foi feita.\n");
    }else{

        novo->dado = valor;
        novo->prox = NULL;
        // primeira vez
        if(fila_vazia(f))
        {
            //inicio seta novo
            f->inicio = novo;
        }else{
            //fim seta proximo novo
            f->fim->prox = novo;
        }
        f->fim = novo;
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
int desempilhar(pilha *p)
{
    int x;
    Tpilha *aux;


        aux = p->topo;
        x = aux->item;
        p->topo = aux->ant;
        aux->ant = NULL;

        free(aux);

        return x;

}

int desenfileirar(fila *f)
{
    int y;
    Tfila *aux2;

    aux2 = f->inicio;
    y = aux2->dado;
    f->inicio = aux2->prox;

    if(f->inicio == NULL)
    {
        f->fim = NULL;
    }
    free(aux2);

    return y;
}
////////////////////////////////////////////////////////////////////////////////////////////////////

void mostra_pilha(pilha *p)
{
    Tpilha *aux = (Tpilha*)malloc(sizeof(Tpilha));

    aux = p->topo;
    while(aux != NULL)
    {
        printf("\n >%d",aux->item);
        aux = aux->ant;
    }
}

void mostra_fila(fila *f)
{
    Tfila *aux = (Tfila*)malloc(sizeof(Tfila));

    aux = f->inicio;

    while(aux != NULL)
    {
        printf(" |%d",aux->dado);
        aux = aux->prox;
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
 void finalizar(fila *f,pilha *p)
 {
    f->inicio = NULL;
    f->fim = NULL;
    p->topo = NULL;
    free(f);
    free(p);
 }

////////////////////////////////////////////////////////////////////////////////////////////////////
void menu()
{
    int valor, opcao, flag;

    pilha *p = (pilha*)malloc(sizeof(pilha));
    if(p == NULL)
    {
        printf("\n alocacao nao foi feita.\n");
    }else{

        criar_pilha(p);
    }
        fila *f = (fila*)malloc(sizeof(fila));
        if(f == NULL)
        {
            printf("\n alocacao nao foi feita.\n");
        }else{

            criar_fila(f);
        }

    do{
        system("cls");
        printf("\n======= Pilha e Fila ===================================\n");
        if(pilha_vazia(p))
        {
            printf("\n Pilha Vazia!!!\n");
            printf("\n Insira.\n\n");
        }else{
            printf("\n\n");
            mostra_pilha(p);
        }
            if(fila_vazia(f))
            {
                printf("\n\n Fila Vazia!!!\n");
                printf("\n Insira.\n");
            }else{
                printf("\n\n\n");
                mostra_fila(f);
            }

        printf("\n\n======= MENU =================================\n\n");

        printf(" 1 - Inserir \n");
        printf(" 2 - Retirar \n");
        printf(" 3 - Retirar da pilha para fila \n");
        printf(" 4 - Sair \n");
        printf("\t Escolha uma opcao..:\t");
        scanf("%d",&opcao);
        printf("\n==================================================\n");

        switch(opcao)
        {
        case 1:
            printf("\n ===== Inserir =============================\n");
            printf("\n 1 - Pilha \t 2 - Fila..:\t");
            scanf("%d",&flag);

            while(flag < 1 || flag > 2)
            {
                printf("\n novamente 1 - Pilha \t 2 - Fila..:\t");
                scanf("%d",&flag);
            }

            printf("\n Qual valor deseja inserir..:\t");
            scanf("%d",&valor);

            if(flag == 1)
            {
                empilhar(p, valor);
            }else{
                enfileirar(f,valor);
            }
            getch();
        break;
        case 2:
            printf("\n ===== Retirar =============================\n");
            printf("\n 1 - Pilha \t 2 - Fila..:\t");
            scanf("%d",&flag);

            while(flag < 1 || flag > 2)
            {
                printf("\n novamente 1 - Pilha \t 2 - Fila..:\t");
                scanf("%d",&flag);
            }

            if(flag == 1)
            {
                if(pilha_vazia(p))
                {
                    printf("\n Pilha Vazia!!!\n");
                    printf("\n Insira.\n\n");
                    }else{
                        desempilhar(p);
                }
            }else{
                if(fila_vazia(f))
                {
                    printf("\n\n Fila Vazia!!!\n");
                    printf("\n Insira.\n");
                }else{
                    desenfileirar(f);
                }
            }
            getch();
        break;
        case 3:
            printf("\n ===== Retirar da pilha para fila =========================\n");
            if(pilha_vazia(p) && fila_vazia(f))
            {
                printf("\n Vazia!!!\n");
                printf("\n Insira.\n\n");
            }else{
                valor = desempilhar(p);
                enfileirar(f,valor);
            }
            break;
        case 4:
            printf("\n ===== finalizar =============================\n");
            finalizar(f,p);
            opcao = 4;
            break;
        }
    }while(opcao != 4);
}

int main()
{
    menu();
    return 0;
}
