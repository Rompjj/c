#include <stdio.h>
#include <stdlib.h>
    typedef struct no{
        int dado;
        struct no* prox;
    }no;

    typedef struct{
        no* inicio;
        no* fim;
    }lista;


void criar(lista* l)
{
    l->fim = NULL;
    l->inicio = NULL;
}

int listaVazia(lista* l)
{
    if(l->inicio == NULL)
    {
        return 1;
    }else{
        return 0;
    }
}

void listar(lista* l, int valor)
{
    no* novo = (no*)malloc(sizeof(no)); //cria nova estrutura de no
    if(novo == NULL)
    {
        printf("\n Alocacao do novo nao foi feita.\n");
    }else{

        novo->dado = valor;
        novo->prox = NULL;

        //crio primeira insercao
        if(listaVazia(l))
        {
            l->inicio = novo;
        }else{
        //segunda insercao
            l->fim->prox = novo;
        }
    }
    //demais insersoes
    l->fim = novo;    //coloca o fim na ultima estrutura no
    novo->prox = l->inicio;   //aponta o ultimo prox para o inicio
}

int remover(lista* l, int valor)
{
    no* aux = (no*)malloc(sizeof(no));
    no* ant = (no*)malloc(sizeof(no));

    if(listaVazia(l))
    {
        printf("\n Lista vazia! \n");
    }else{
        aux = l->inicio;
        ant = l->inicio;
    }

    do{
        if(aux->dado == valor) //achou o valor consultado
        {
            if(aux == l->inicio)       //se o elemento estiver no inicio
            {
                l->inicio = aux->prox;
                l->fim->prox = aux->prox;
                free(aux);
                return 1;
            }else{      //caso nao seja o primeiro da lista
                ant->prox = aux->prox;
                if(aux == l->fim)
                {
                    l->fim = ant;
                }
                free(aux);
                return 1;
                }
            }else{
                ant = aux;
                aux = aux->prox;
            }
    }while(aux != l->inicio);
}

void mostrar(lista* l)
{
    if(listaVazia(l))
    {
        printf("\n Lista vazia.\n");
    }else{
        no* aux = (no*)malloc(sizeof(no));
        aux = l->inicio;

        do{
            printf("| %d",aux->dado);
            aux = aux->prox;
        }while(aux != l->inicio);
    }
}

void consultar(lista* l, int valor)
{
    no* aux=(no*)malloc(sizeof(no));
    int achou = 0;

    if(listaVazia(l))
    {
        printf("\n Lista vazia.\n");
    }else{
        aux = l->inicio;

        do{
            if(aux->dado == valor)
            {
                printf("\n Valor %d encontrado.\n",aux->dado);
                achou = 1;
            }else{
                aux = aux->prox;
            }
        }while(aux != l->inicio && achou == 0);

        if(achou == 0)
        {
            printf("\n valor nao encontrado.\n");
        }
    }
}


void menu()
{
    lista* li = (lista*)malloc(sizeof(lista));
    lista* liB = (lista*)malloc(sizeof(lista));
    if(li == NULL)
    {
        printf("\n Alocacao li nao foi feita.\n");
    }else{
        criar(li);
    }

    int opcao,valor;

    do{
        system("cls");
            printf("\n Lista ========================== \n");
            mostrar(li);
            printf("\n\n ================= Menu =================== \n");
            printf("1 - Inserir elemento lista \n");
            printf("2 - Buscar  elemento lista \n");
            printf("3 - Remover elemento lista \n");
            printf("4 - Organizar lista Circular \n");
            printf("0 - Sair \n");

            printf("\n Qual opcao deseja:. ");
            scanf("%d",&opcao);

            switch(opcao)
            {
                case 1:
                    printf("\n ============== Listar ===================\n");
                    printf("Informe o valor: ");
                    scanf("%d",&valor);

                    listar(li,valor);
                    getch();
                break;
                case 2:
                    printf("\n ============== Consulta ===================\n");
                    printf("Informe o valor que deseja consultar: ");
                    scanf("%d",&valor);

                    consultar(li,valor);
                    getch();
                break;
                case 3:
                    printf("\n ============== Remover ===================\n");
                    printf("Informe o valor que deseja remover: ");
                    scanf("%d",&valor);

                    remover(li,valor);
                    getch();
                break;
                case 4:
                    printf("\n ============== Ordenar  ===================\n");

                   // ordenar(li);
                    //criar(lib)
                    //inserir(nova lista)
                break;
                case 0:
                    opcao = 0;
                break;
            }
        }while(opcao != 0);
}

int main()
{
    menu();
    return 0;
}
